/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httye;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import sun.management.Agent;

/**
 *
 * @author Ramdani
 */
public class Generator implements MouseListener, MouseMotionListener {
    public JButton gr[];
    public JPanel salah, benar;
    public int indeks,count,in=0;
    public JLabel instruksi;
    public boolean[] banding;
    public JPanel papanGame1;
    
    public Generator () {
//        initComponents();
        papanGame1=new JPanel();
//        papanGame1.setAlignmentX(JComponent.CENTER_ALIGNMENT);
//        papanGame1.setMaximumSize(new Dimension(250,300));
//        papanGame1.setBackground(Color.red);
//        HTTYE.gb.add(papanGame1, BorderLayout.CENTER);
//        papanGame1.add(JComponent.CENTER_ALIGNMENT);
//        papanGame1.set
        
//        Dimension layar=getSize();
//        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
//        Point centerPoint = ge.getCenterPoint();
//        int dx = centerPoint.x - layar.width / 2;
//        int dy = centerPoint.y - layar.height / 2; 
//        setLocation(dx, dy);
    }
    
    public static void shuffleArray(int [] ar) {
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length-1; i > 0; i--)
        {
          int index = rnd.nextInt(i + 1);
          // Simple swap
          int a = ar[index];
          ar[index] = ar[i];
          ar[i] = a;
        }
      }
    
    public void setLvl1G1 () {
        HTTYE.gb.level.setText("1");
        HTTYE.gb.time.setText("20");
        //Nambah Panel  
        papanGame1.setBounds(200, 90, 250, 300);
//        papanGame1.setBackground(new Color(0,0,0,0));
        papanGame1.setPreferredSize(new Dimension(250,300));
        
//        instruksi=new JLabel("Urutkan Angka dari yang terkecil");
//        HTTYE.gb.jPanel1.add(instruksi);
//        instruksi.setBounds(150, 10, 20, 100);
        
        int[] arr={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        gr=new JButton[arr.length];
        
        shuffleArray(arr);
        
        for (int j=0;j<arr.length;j++) {
            System.out.print(arr[j]+" ");
        }
        System.out.println("Bisa bangsu");
        
        //bikin kotak
        banding=new boolean[arr.length];
        for(int i=0;i<arr.length;i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
            banding[i]=true;
        }
        
        HTTYE.gb.add(papanGame1);
        
        //method setiap tombol
        for (int i = 0; i < arr.length; i++) {
            String tmp=Integer.toString(arr[i]);
            gr[i].setText(tmp);
//            System.out.println(tmp);

            gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g1) {
                        System.out.println(tmp);
                        int nil=Integer.valueOf(tmp);
                        int ban=0, in=0;
                        boolean cek=true;
                        
                        //Pengecekkan
                        for(int j=0;j<arr.length;j++) {
                            ban=arr[j];
                            if(banding[j]==true) {
                                if(ban<nil) {   
                                    cek=false;
                                    banding[j]=true;
                                    break;
                                }
                                if (ban==nil) {
                                    in=j;
                                }
                            }
                        }
                        if (cek==true) {
                            banding[in]=false;
                            arr[in]=0;
                            gr[in].setEnabled(false);
                            gr[in].setText("");
                            
                            System.out.println("benar");
                        } else {
                            System.out.println("salah");
                        }
                        
                        //Finally
                        boolean lanjut=false;
                        for(int x=0;x<arr.length;x++) {
                            if(banding[x]==true) {
                                lanjut=false;
                                break;
                            } else {
                                lanjut=true;
                                continue;
                            }
                        }
                        if(lanjut==true) {
                            papanGame1.removeAll();
                            HTTYE.kode=11;
                            HTTYE.waktu=Integer.valueOf(HTTYE.gb.time.getText());
                            System.out.println("waktu : "+HTTYE.waktu);
                            if(HTTYE.waktu>=20) {
//                                HTTYE.reward=2;
                                HTTYE.bintang=new ImageIcon(this.getClass().getResource("/httye/images/starReward.png"));
                                HTTYE.rew.sBRew1.setIcon(HTTYE.bintang);
                                HTTYE.rew.sBRew2.setIcon(HTTYE.bintang);
                            }
                            HTTYE.rew.setVisible(true);
                        }
                    }
                });
        }
        HTTYE.gb.setVisible(true);
        
    }
    
    public void setLvl2G1 () {
        //reset Reward
        HTTYE.resetBintang=new ImageIcon(this.getClass().getResource("/httye/images/starBlackReward.png"));
        HTTYE.rew.sBRew1.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew2.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew3.setIcon(HTTYE.resetBintang);
        
        HTTYE.gb.level.setText("2");
        //Nambah Panel  
        papanGame1.setBounds(160, 90, 300, 300);
//        papanGame1.setBackground(new Color(0,0,0,0));
        papanGame1.setPreferredSize(new Dimension(250,300));
        
//        instruksi=new JLabel("Urutkan Angka dari yang terkecil");
//        HTTYE.gb.jPanel1.add(instruksi);
//        instruksi.setBounds(150, 10, 20, 100);
        
        int[] arr={1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39,41,43,45,47,49};
        gr=new JButton[arr.length];
        
        shuffleArray(arr);
        
        for (int j=0;j<arr.length;j++) {
            System.out.print(arr[j]+" ");
        }
        System.out.println("Bisa bangsu");
        
        //bikin kotak
        banding=new boolean[arr.length];
        for(int i=0;i<arr.length;i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
            banding[i]=true;
        }
        
        HTTYE.gb.add(papanGame1);
        
        //method setiap tombol
        for (int i = 0; i < arr.length; i++) {
            String tmp=Integer.toString(arr[i]);
            gr[i].setText(tmp);
//            System.out.println(tmp);

            gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g1) {
                        System.out.println(tmp);
                        int nil=Integer.valueOf(tmp);
                        int ban=0, in=0;
                        boolean cek=true;
                        
                        //Pengecekkan
                        for(int j=0;j<arr.length;j++) {
                            ban=arr[j];
                            if(banding[j]==true) {
                                if(ban<nil) {   
                                    cek=false;
                                    banding[j]=true;
                                    break;
                                }
                                if (ban==nil) {
                                    in=j;
                                }
                            }
                        }
                        if (cek==true) {
                            banding[in]=false;
                            arr[in]=0;
                            gr[in].setEnabled(false);
                            gr[in].setText("");
                            
                            System.out.println("benar");
                        } else {
                            System.out.println("salah");
                        }
                        
                        //Finally
                        boolean lanjut=false;
                        for(int x=0;x<arr.length;x++) {
                            if(banding[x]==true) {
                                lanjut=false;
                                break;
                            } else {
                                lanjut=true;
                                continue;
                            }
                        }
                        if(lanjut==true) {
                            papanGame1.removeAll();
                            HTTYE.kode=21;
                            HTTYE.rew.setVisible(true);
                        }
                    }
                });
        }
        HTTYE.gb.setVisible(true);
        
    }
    
    public void setLvl3G1 () {
        //reset Reward
        HTTYE.resetBintang=new ImageIcon(this.getClass().getResource("/httye/images/starBlackReward.png"));
        HTTYE.rew.sBRew1.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew2.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew3.setIcon(HTTYE.resetBintang);
        
        HTTYE.gb.level.setText("3");
        //Nambah Panel  
        papanGame1.setBounds(160, 90, 300, 300);
//        papanGame1.setBackground(new Color(0,0,0,0));
        papanGame1.setPreferredSize(new Dimension(250,300));
        
//        instruksi=new JLabel("Urutkan Angka dari yang terkecil");
//        HTTYE.gb.jPanel1.add(instruksi);
//        instruksi.setBounds(150, 10, 20, 100);
        
        int[] arr={3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,75};
        gr=new JButton[arr.length];
        
        shuffleArray(arr);
        
        for (int j=0;j<arr.length;j++) {
            System.out.print(arr[j]+" ");
        }
        System.out.println("Bisa bangsu");
        
        //bikin kotak
        banding=new boolean[arr.length];
        for(int i=0;i<arr.length;i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
            banding[i]=true;
        }
        
        HTTYE.gb.add(papanGame1);
        
        //method setiap tombol
        for (int i = 0; i < arr.length; i++) {
            String tmp=Integer.toString(arr[i]);
            gr[i].setText(tmp);
//            System.out.println(tmp);

            gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g1) {
                        System.out.println(tmp);
                        int nil=Integer.valueOf(tmp);
                        int ban=0, in=0;
                        boolean cek=true;
                        
                        //Pengecekkan
                        for(int j=0;j<arr.length;j++) {
                            ban=arr[j];
                            if(banding[j]==true) {
                                if(ban<nil) {   
                                    cek=false;
                                    banding[j]=true;
                                    break;
                                }
                                if (ban==nil) {
                                    in=j;
                                }
                            }
                        }
                        if (cek==true) {
                            banding[in]=false;
                            arr[in]=0;
                            gr[in].setEnabled(false);
                            gr[in].setText("");
                            
                            System.out.println("benar");
                        } else {
                            System.out.println("salah");
                        }
                        
                        //Finally
                        boolean lanjut=false;
                        for(int x=0;x<arr.length;x++) {
                            if(banding[x]==true) {
                                lanjut=false;
                                break;
                            } else {
                                lanjut=true;
                                continue;
                            }
                        }
                        if(lanjut==true) {
                            papanGame1.removeAll();
                            HTTYE.kode=31;
                            HTTYE.rew.setVisible(true);
                        }
                    }
                });
        }
        HTTYE.gb.setVisible(true);
        
    }
    
    public void setLvl4G1 () {
        //reset Reward
        HTTYE.resetBintang=new ImageIcon(this.getClass().getResource("/httye/images/starBlackReward.png"));
        HTTYE.rew.sBRew1.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew2.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew3.setIcon(HTTYE.resetBintang);
        
        HTTYE.gb.level.setText("4");
        //Nambah Panel  
        papanGame1.setBounds(120, 90, 400, 400);
//        papanGame1.setBackground(new Color(0,0,0,0));
        papanGame1.setPreferredSize(new Dimension(250,300));
        
//        instruksi=new JLabel("Urutkan Angka dari yang terkecil");
//        HTTYE.gb.jPanel1.add(instruksi);
//        instruksi.setBounds(150, 10, 20, 100);
        
        int[] arr={1,7,13,19,25,31,37,43,49,55,61,67,73,79,85,91,97,103,109,115,121,127,133,139,145,1,1,1,1,1,2,2,2,2,2,2};
        gr=new JButton[arr.length];
        
        shuffleArray(arr);
        
        for (int j=0;j<arr.length;j++) {
            System.out.print(arr[j]+" ");
        }
        System.out.println("Bisa bangsu");
        
        //bikin kotak
        banding=new boolean[arr.length];
        for(int i=0;i<arr.length;i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(55, 55));
            papanGame1.add(gr[i]);
            banding[i]=true;
        }
        
        HTTYE.gb.add(papanGame1);
        
        //method setiap tombol
        for (int i = 0; i < arr.length; i++) {
            String tmp=Integer.toString(arr[i]);
            gr[i].setText(tmp);
//            System.out.println(tmp);

            gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g1) {
                        System.out.println(tmp);
                        int nil=Integer.valueOf(tmp);
                        int ban=0, in=0;
                        boolean cek=true;
                        
                        //Pengecekkan
                        for(int j=0;j<arr.length;j++) {
                            ban=arr[j];
                            if(banding[j]==true) {
                                if(ban<nil) {   
                                    cek=false;
                                    banding[j]=true;
                                    break;
                                }
                                if (ban==nil) {
                                    in=j;
                                }
                            }
                        }
                        if (cek==true) {
                            banding[in]=false;
                            arr[in]=0;
                            gr[in].setEnabled(false);
                            gr[in].setText("");
                            
                            System.out.println("benar");
                        } else {
                            System.out.println("salah");
                        }
                        
                        //Finally
                        boolean lanjut=false;
                        for(int x=0;x<arr.length;x++) {
                            if(banding[x]==true) {
                                lanjut=false;
                                break;
                            } else {
                                lanjut=true;
                                continue;
                            }
                        }
                        if(lanjut==true) {
                            papanGame1.removeAll();
                            HTTYE.kode=41;
                            HTTYE.rew.setVisible(true);
                        }
                    }
                });
        }
        HTTYE.gb.setVisible(true);
        
    }
    
    public void setLvl5G1 () {
        //reset Reward
        HTTYE.resetBintang=new ImageIcon(this.getClass().getResource("/httye/images/starBlackReward.png"));
        HTTYE.rew.sBRew1.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew2.setIcon(HTTYE.resetBintang);
        HTTYE.rew.sBRew3.setIcon(HTTYE.resetBintang);
        
        HTTYE.gb.level.setText("5");
        //Nambah Panel  
        papanGame1.setBounds(120, 90, 400, 400);
//        papanGame1.setBackground(new Color(0,0,0,0));
        papanGame1.setPreferredSize(new Dimension(250,300));
        
//        instruksi=new JLabel("Urutkan Angka dari yang terkecil");
//        HTTYE.gb.jPanel1.add(instruksi);
//        instruksi.setBounds(150, 10, 20, 100);
        
//        int[] arr={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53};
        int[] arr={1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,4,4,4,4,4,5,5,5,5,5,6,6,6,6,6,7,7,7,7,7,8,8,8,8,8,0,0};
        gr=new JButton[arr.length];
        
        shuffleArray(arr);
        
        for (int j=0;j<arr.length;j++) {
            System.out.print(arr[j]+" ");
        }
        System.out.println("Bisa bangsu");
        
        //bikin kotak
        banding=new boolean[arr.length];
        for(int i=0;i<arr.length;i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
            banding[i]=true;
        }
        
        HTTYE.gb.add(papanGame1);
        
        //method setiap tombol
        for (int i = 0; i < arr.length; i++) {
            String tmp=Integer.toString(arr[i]);
            gr[i].setText(tmp);
//            System.out.println(tmp);

            gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g1) {
                        System.out.println(tmp);
                        int nil=Integer.valueOf(tmp);
                        int ban=0, in=0;
                        boolean cek=true;
                        
                        //Pengecekkan
                        for(int j=0;j<arr.length;j++) {
                            ban=arr[j];
                            if(banding[j]==true) {
                                if(ban<nil) {   
                                    cek=false;
                                    banding[j]=true;
                                    break;
                                }
                                if (ban==nil) {
                                    in=j;
                                }
                            }
                        }
                        if (cek==true) {
                            banding[in]=false;
                            arr[in]=0;
                            gr[in].setEnabled(false);
                            gr[in].setText("");
                            
                            System.out.println("benar");
                        } else {
                            System.out.println("salah");
                        }
                        
                        //Finally
                        boolean lanjut=false;
                        for(int x=0;x<arr.length;x++) {
                            if(banding[x]==true) {
                                lanjut=false;
                                break;
                            } else {
                                lanjut=true;
                                continue;
                            }
                        }
                        if(lanjut==true) {
                            HTTYE.kode=51;
                            papanGame1.removeAll();
                            HTTYE.rew.backRew.setEnabled(false);
                            HTTYE.rew.nextRew.setText("Finish");
                            HTTYE.rew.setVisible(true);
                        }
                    }
                });
        }
        HTTYE.gb.setVisible(true);
        
    }

    public void setLvl1G2 () {
//        instruksi=new JLabel("Cari angka 2");
//        instruksi.setFont(new java.awt.Font("Bebas Neue", 1, 24));
//        instruksi.setBounds(250, 30, 100, 30);
//        HTTYE.gb.jPanel1.add(instruksi);
//        System.out.println("FUCK");
        //waktu
//        count=20;
//        int ct=Integer.parseInt(HTTYE.gb.time.getText());
//        Timer waktu=new Timer();
//        TimerTask task=new TimerTask() {
//            @Override
//            public void run() {
//                System.out.println("oke");
//            }
//        };
//        waktu.scheduleAtFixedRate(task, ct, 500);
        HTTYE.gb.level.setText("1");
        //Nambah Panel
        papanGame1.setBounds(200, 90, 250, 300);
        papanGame1.setPreferredSize(new Dimension(250,300));
        
        indeks=16;
        gr=new JButton[indeks];
        
        for (int i=0; i<indeks; i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
        }
        
        HTTYE.gb.add(papanGame1);
        Random ran=new Random();
        int car=ran.nextInt(indeks);
        
        for (int i=0;i<indeks;i++) {
            if(i==car) {
                gr[i].setText("2");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g2) {
                        HTTYE.kode=12;
                        papanGame1.removeAll();
                        HTTYE.rew.setVisible(true);
                    }
                });
                continue;
            } else {
                gr[i].setText("7");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l1g2) {
                        System.out.println("Coba salah");
                    }
                });
            }
        }
    }
    
    public void setLvl2G2 () {
        HTTYE.gb.level.setText("2");
        //Nambah Panel
        papanGame1.setBounds(160, 90, 300, 300);
        papanGame1.setPreferredSize(new Dimension(250,300));
        
        indeks=25;
        gr=new JButton[indeks];
        
        for (int i=0; i<indeks; i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
        }
        
        HTTYE.gb.add(papanGame1);
        Random ran=new Random();
        int car=ran.nextInt(indeks);
        
        for (int i=0;i<indeks;i++) {
            if(i==car) {
                gr[i].setText("V");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l2g2) {
                        HTTYE.kode=22;
                        papanGame1.removeAll();
                        HTTYE.rew.setVisible(true);
                    }
                });
                continue;
            } else {
                gr[i].setText("U");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l2g2) {
                        System.out.println("Coba salah");
                    }
                });
            }
        }
    }
    
    public void setLvl3G2 () {
        HTTYE.gb.level.setText("3");
        //Nambah Panel
        papanGame1.setBounds(160, 90, 300, 300);
        papanGame1.setPreferredSize(new Dimension(250,300));
        
        indeks=25;
        gr=new JButton[indeks];
        
        for (int i=0; i<indeks; i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
        }
        
        HTTYE.gb.add(papanGame1);
        Random ran=new Random();
        int car=ran.nextInt(indeks);
        
        for (int i=0;i<indeks;i++) {
            if(i==car) {
                gr[i].setText("O");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l3g2) {
                        HTTYE.kode=32;
                        papanGame1.removeAll();
                        HTTYE.rew.setVisible(true);
                    }
                });
                continue;
            } else {
                gr[i].setText("Q");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l3g2) {
                        System.out.println("Coba salah");
                    }
                });
            }
        }
    }
    public void setLvl4G2 () {
        HTTYE.gb.level.setText("4");
        //Nambah Panel
        papanGame1.setBounds(120, 90, 400, 400);
        papanGame1.setPreferredSize(new Dimension(250,300));
        
        indeks=36;
        gr=new JButton[indeks];
        
        for (int i=0; i<indeks; i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
        }
        
        HTTYE.gb.add(papanGame1);
        Random ran=new Random();
        int car=ran.nextInt(indeks);
        
        for (int i=0;i<indeks;i++) {
            if(i==car) {
                gr[i].setText("!");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l4g2) {
                        HTTYE.kode=42;
                        papanGame1.removeAll();
                        HTTYE.rew.setVisible(true);
                    }
                });
                continue;
            } else {
                gr[i].setText("i");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l4g2) {
                        System.out.println("Coba salah");
                    }
                });
            }
        }
    }
    
    public void setLvl5G2 () {
        HTTYE.gb.level.setText("5");
        //Nambah Panel
        papanGame1.setBounds(140, 90, 350, 400);
        papanGame1.setPreferredSize(new Dimension(250,300));
        
        indeks=36;
        gr=new JButton[indeks];
        
        for (int i=0; i<indeks; i++) {
            gr[i]=new JButton();  
            gr[i].setBackground(Color.white);
            gr[i].setPreferredSize(new Dimension(50, 50));
            papanGame1.add(gr[i]);
        }
        
        HTTYE.gb.add(papanGame1);
        Random ran=new Random();
        int car=ran.nextInt(indeks);
        
        for (int i=0;i<indeks;i++) {
            if(i==car) {
                gr[i].setText("l");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l5g2) {
                        HTTYE.kode=52;
                        papanGame1.removeAll();
                        HTTYE.rew.backRew.setEnabled(false);
                        HTTYE.rew.nextRew.setText("Finish");
                        HTTYE.rew.setVisible(true);
                    }
                });
                continue;
            } else {
                gr[i].setText("1");
                gr[i].addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent l5g2) {
                        System.out.println("Coba salah");
                    }
                });
            }
        }
    }
    

    @Override
    public void mouseClicked(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        int l=e.getClickCount();
        System.out.println("ccchchch");
    }

    @Override
    public void mousePressed(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        int l=e.getModifiers();
        System.out.println("kjhk");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("kjhk");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        System.out.println("kjhk");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
        System.out.println("kjhk");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        System.out.println("kjhk");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        System.out.println("kjhk");
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void initComponents() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


}
